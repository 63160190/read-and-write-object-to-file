/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class WriteFriend {
    public static void main(String[] args) {
        FileOutputStream fos = null;
        try {
            Friend friend1 = new Friend("Chutiphong",20,"0881543959");
            Friend friend2 = new Friend("Nick",20,"0881543951");
            Friend friend3 = new Friend("Nut",20,"0881543952");
            //friends.dat
            File file = new File("friend.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos =new ObjectOutputStream(fos);
            oos.writeObject(friend1);
            oos.writeObject(friend2);
            oos.writeObject(friend3);            
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fos.close();
            } catch (IOException ex) {
                Logger.getLogger(WriteFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        
    }
}
