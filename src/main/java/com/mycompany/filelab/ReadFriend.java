package com.mycompany.filelab;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author User
 */
public class ReadFriend {
    public static void main(String[] args) {
        FileInputStream fis = null;
        try {
            Friend friend1 = null;
            Friend friend2 = null;
            Friend friend3 = null;
            File file =new File("Friends.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
           friend1 = (Friend) ois.readObject();
           friend2 = (Friend) ois.readObject();
           friend3 = (Friend) ois.readObject();
            System.out.println(friend1);
            System.out.println(friend2);
            System.out.println(friend3);
                      
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                fis.close();
            } catch (IOException ex) {
                Logger.getLogger(ReadFriend.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
