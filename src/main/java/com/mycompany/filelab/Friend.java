/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.filelab;

import java.io.Serializable;

/**
 *
 * @author User
 */
public class Friend implements Serializable {
    private String name;
    private int age;
    private String tel;

    

    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public String getTel() {
        return tel;
    }

    public Friend(String name, int age, String tel) {
        this.name = name;
        this.age = age;
        this.tel = tel;
    }
    
    @Override
    public String toString() {
        return "Friend{" + "name=" + name + ", age=" + age + ", tel=" + tel + '}';
    }
    
}
